var unirest = require('unirest');

exports.handler = async (event, context, callback) => {
    await getWeatherInfo("Nottingham,uk").then((weatherResponse) => {
        callback(null, buildSuccessfulResponse(weatherResponse.body))
    })
};

function getWeatherInfo(cityName) {
    return new Promise(function (resolve, reject) {
        unirest.get(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=046e7da6bda66c986ae854b78ebcf68f&units=metric`).then(output => resolve(output))
    })
}

function buildSuccessfulResponse(result) {
    const response = {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        },
        "body": JSON.stringify({
            "weather_main": result.weather[0].main,
            "main_temp": result.main.temp,
            "main_feels_like": result.main.feels_like,
            "main_max_temp": result.main.temp_max,
            "main_min_temp": result.main.temp_min,
            "main_pressure": result.main.pressure,
            "main_humidity": result.main.humidity,
            "sunrise": epochToTime(result.sys.sunrise),
            "sunset": epochToTime(result.sys.sunset),
            "wind": result.wind.speed,
            "visibility": result.visibility
        }),
        "isBase64Encoded": false
    }
    return response;
}

function epochToTime(E) {
    var time = new Date(E * 1000)
    var hours = time.getHours()
    var minutes = (time.getMinutes() == 0) ? "00" : time.getMinutes()
    return `${hours}:${minutes}`
}
