# README #

## Coding Exercise
1. Create a simple API Gateway Lambda that returns today's weather for Nottingham, UK using whatever weather service you choose.
2. Create a Single Page Application (SPA) that queries the API GW Lambda and displays the results. 
3. Create a CloudFormation Custom Resource that can
  - Deploy the API GW project
  - Create an S3 bucket configured for static website hosting.
  - CONFIGURE the SPA project to interact with the API GW. Deploy it to the S3 bucket.
  - Return the S3 static website URL to CloudFormation as an output variable
     

## Comments:
- The point of this exercise is to create a deployment process that is executable from CloudFormation. The API GW and SPA can be trivial.
- Focus on creating a robust CloudFormation Custom Resource that can deploy both project and wire them up correctly. Dont forget to report errors back to CloudFormation. Make sure it handles creates/updates/deletes.
- SPA should be implemented in React or Angular
- The other resources can be implemented using AWS SAM, Serverless Framework, or CDK.
- Preferred language is TypeScript, but plain NodeJs and Python is also acceptable.
- Be creative. Show off. Make it interesting.

## Solution 
1. Created SPA for Weather Monitoring ( Live Version is here : https://ttec-weather-12345.s3.eu-west-2.amazonaws.com/index.html)
2. Consumed Open Weather API from lambda and exposed the endpoint from API-gateway for Frontend ( Live Version : https://cyjzjq0l97.execute-api.eu-west-2.amazonaws.com/call) 
 Response from API will be like this:   
 ```
      {
      "weather_main": "Clear",
      "main_temp": 7.15,
      "main_feels_like": 2.48,
      "main_max_temp": 7.78,
      "main_min_temp": 6.67,
      "main_pressure": 1006,
      "main_humidity": 87,
      "sunrise": "8:11",
      "sunset": "15:48",
      "wind": 5.1,
      "visibility": 10000
     } 
 ```

3. Cloud Formation Stack is created to ease the process of resource creation and divided in three seperate part 

- Step 1 : Code Bucket Creation ==> This will create a bucket in which Lambda zip code have to be moved
- Step 2 : Once Code is moved to the bucket, second cloudformation template is to be played which will create api-gateway and place proper lambda code and also create roles. 
- Step 3 : This will create static website hosting bucket with all the proper bucket policy and permission. Once this is created you have to move the everything inside /frontend folder as it is and your website should be working by now . You can find the url from the output section from the third template 

## How to Deploy using cloudformation template provided
1. use template 1 and run the template provide the bucket name from the parameter and note down the bucket or you can refer from the output section as well 
2. Download the code base from the bitbucket, go to the backend folder in the project and create the zip file for all the files in **/backend/get-weather-info/** and upload in the bucket you created in step 1
3. Now its time to run Template 2, One of the field to fill is Code_bucket_name(s3BucketName) and zip_file_name (zipFileName), Please provide the right information in these parameter field
4. Once Step 2 Is complete from the output section you will receive API Gateway Invoke URL. Note this down 
5. Go to the frontend code and update this url in index.html file ( !!! Important )
6. Move to Step 3, Here run the template 3 and provide the static website bucket name ( keep it unique ). 
7. Once done with Step 3 upload entire frontent directory which includes assets folder and index.html file. This should conclude your deployment process
8. Find the Website URL from the output section of Template 3 and do not forget to add /index.html in the end
## Note : Make sure to use /index.html in the end of the URL

